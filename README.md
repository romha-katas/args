# Kata Args

Subject from [Coding Dojo](https://codingdojo.org/kata/Args/)

## What we studied during this kata:

- How to mob
- How/Why we focus on the domain and business before the technical stuff
- How to use extension functions to improve our tests
- How to use First-class (citizen) functions and Higher Order functions
- Open/Closed


## What we will study during the next session:

- Dependencies inversion
- Generics?
- Tests crafting (DSL & Test helpers)

