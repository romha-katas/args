package io.thalesdigital

import io.thalesdigital.arguments.ProgramArguments
import io.thalesdigital.schema.Flag
import io.thalesdigital.schema.FlagType
import io.thalesdigital.schema.Schema
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class CommandArgumentIT {

    @Test
    fun `should retrieve flag according to the schema`() {
        Schema().with(Flag("p", FlagType.Integer))
                .with(Flag("l", FlagType.Boolean))
                .with(Flag("x", FlagType.Boolean))
            .`applied_to`("-p 12 -l")
            .`should produce`(
                "p".integer(12),
                "l".boolean(true),
                "x".boolean(false),
            )
    }

    @Test
    fun `should raise an error if the value does not match the flag type`() {
        Schema().with(Flag("p", FlagType.Integer))
            .with(Flag("l", FlagType.Boolean))
            .with(Flag("x", FlagType.Boolean))
            .`applied_to`("-p abc -l")
            .`should raise an error`<ArgumentResolverException>()
    }


    private fun Schema.`applied_to`(s: String): TestHelper {
        return TestHelper(this, s)
    }

    class TestHelper(val schema: Schema, val commandLine: String) {
        val expected = mutableListOf<ExpectedValue>()

        fun `should produce`(
            vararg expected: ExpectedValue
        ) {
            val results = GetParameters(schema)(commandLine)
            expected.forEach {
                it.check(results)
            }
        }

        inline fun <reified T: RuntimeException> `should raise an error`() {
            try {
                GetParameters(schema)(commandLine)
                Assertions.fail("Expecting an exception")
            } catch(e: RuntimeException) {
                Assertions.assertTrue(e is T, "Expecting a ${T::class.qualifiedName} exception. But got a ${e::class.qualifiedName}")
            }
        }
    }

    private fun String.integer(value: Int): ExpectedValue {
        return ExpectedInteger(this, value)
    }

    private fun String.boolean(value: Boolean): ExpectedValue {
        return ExpectedBoolean(this, value)
    }

    interface ExpectedValue {
        fun check(results: ProgramArguments)
    }
    class ExpectedInteger(val name: String, val value: Int): ExpectedValue {
        override fun check(results: ProgramArguments) {
            Assertions.assertEquals(value, results.asInteger(Flag(name, FlagType.Integer)))
        }
    }

    class ExpectedBoolean(val name: String, val value: Boolean): ExpectedValue {
        override fun check(results: ProgramArguments) {
            Assertions.assertEquals(value, results.asBoolean(Flag(name, FlagType.Boolean)))
        }
    }

}
