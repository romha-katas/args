package io.thalesdigital

import io.thalesdigital.resolver.Argument
import io.thalesdigital.resolver.ArgumentResolver
import io.thalesdigital.resolver.CommandLine
import io.thalesdigital.arguments.ProgramArguments
import io.thalesdigital.schema.Flag
import io.thalesdigital.schema.FlagType
import io.thalesdigital.schema.Schema
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ArgumentResolverTest {

    @Test
    fun should_accept_no_parameter_for_empty_schema() {
        val commandLineArguments = CommandLine()
        val result = ArgumentResolver(Schema()).invoke(commandLineArguments)
        result.fold(
            {Assertions.fail(it.toString())},
            {}
        )
    }

    @Test
    fun should_set_flag_to_false_if_not_present() {
        val commandLineArguments = CommandLine()
        val result = ArgumentResolver(Schema().withBoolean("p")).invoke(commandLineArguments)
        result.fold(
            {Assertions.fail(it.toString())},
            {arguments -> Assertions.assertFalse(arguments.asBoolean("p"))}
        )
    }

    @Test
    fun should_set_flag_to_true_if_present() {
        val commandLineArguments = CommandLine().with("-p")
        val result = ArgumentResolver(Schema().withBoolean("p")).invoke(commandLineArguments)
        result.fold(
            {Assertions.fail(it.toString())},
            {arguments -> Assertions.assertTrue(arguments.asBoolean("p"))}
        )
    }

    @Test
    fun should_set_flag_to_true_if_present2() {
        val commandLineArguments = CommandLine().with("-p")
        val result = ArgumentResolver(Schema().withBoolean("p")).invoke(commandLineArguments)
        result.fold(
            {Assertions.fail(it.toString())},
            {arguments -> Assertions.assertEquals(0, arguments.asInteger("p"))}
        )
    }

    @Test
    fun should_set_integer_flag_to_0_not_present() {
        val commandLineArguments = CommandLine()

        val result = ArgumentResolver(Schema().withInteger("p")).invoke(commandLineArguments)
        result.fold(
            {Assertions.fail(it.toString())},
            {arguments -> Assertions.assertEquals(0, arguments.asInteger("p"))}
        )
    }

    @Test
    fun should_accept_integer_flag() {
        val commandLineArguments = CommandLine().with("-p 12")
        val result = ArgumentResolver(Schema().withInteger("p")).invoke(commandLineArguments)
        result.fold(
            {Assertions.fail(it.toString())},
            {arguments -> Assertions.assertEquals(12, arguments.asInteger("p"))}
        )

    }

    @Test
    fun should_reject_integer_flag_with_wrong_argument_type() {
        val commandLineArguments = CommandLine().with("-p foobar")
        val result = ArgumentResolver(Schema().withInteger("p")).invoke(commandLineArguments)
        result.fold(
            {},
            {Assertions.fail("Expecting to get an error. But got a valid value")}
        )
    }

    @Test
    fun should_reject_flag_if_not_prefixed_by_dash() {
        val commandLineArguments = CommandLine().with("p")
        val result = ArgumentResolver(Schema().withInteger("p")).invoke(commandLineArguments)
        result.fold(
            {},
            {Assertions.fail("Expecting to get an error. But got a valid value")}
        )
    }

    @Test
    fun should_accept_a_list_of_values() {
        val commandLineArguments = CommandLine().with("-p 12,13,14")
        val result = ArgumentResolver(Schema().withIntegers("p")).invoke(commandLineArguments)
        result.fold(
            {Assertions.fail(it.toString())},
            {arguments -> Assertions.assertEquals(listOf(12, 13, 14), arguments.asIntegers("p"))}
        )
    }

    @Test
    fun should_get_an_empty_list_by_default() {
        val commandLineArguments = CommandLine()
        val result = ArgumentResolver(Schema().withIntegers("p")).invoke(commandLineArguments)
        result.fold(
            {Assertions.fail(it.toString())},
            {arguments -> Assertions.assertEquals(listOf<Int>(), arguments.asIntegers("p"))}
        )
    }

    @Test
    fun should_reject_a_list_with_wrong_values() {
        val commandLineArguments = CommandLine().with("-p 12,foobar,14")
        val result = ArgumentResolver(Schema().withIntegers("p")).invoke(commandLineArguments)
        result.fold(
            {},
            {Assertions.fail("Expecting to get an error. But got a valid value")}
        )
    }


    private fun CommandLine.with(s: String): CommandLine {
        var commandLine = this
        s.split(" ").forEach {
            //commandLine = commandLine.with(Argument(it))
            commandLine = if (it.startsWith("-")) {
                commandLine.with(Argument.option(s.subSequence(1, it.length).toString()))
            } else {
                commandLine.with(Argument.value(it))
            }
        }
        return commandLine
    }

    private fun Schema.withInteger(s: String): Schema {
        return this.with(Flag(s, FlagType.Integer))
    }

    private fun Schema.withIntegers(s: String): Schema {
        return this.with(Flag(s, FlagType.Integers))
    }

    private fun Schema.withBoolean(s: String): Schema {
        return this.with(Flag(s, FlagType.Boolean))
    }

    private fun ProgramArguments.asInteger(s: String): Int {
        return this.asInteger(Flag(s, FlagType.Integer))
    }

    private fun ProgramArguments.asIntegers(s: String): List<Int> {
        return this.asIntegerList(Flag(s, FlagType.Integers))
    }

    private fun ProgramArguments.asBoolean(s: String): Boolean {
        return this.asBoolean(Flag(s, FlagType.Boolean))
    }
}
