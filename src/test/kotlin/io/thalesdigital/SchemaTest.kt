package io.thalesdigital

import io.thalesdigital.resolver.Argument
import io.thalesdigital.resolver.CommandLine
import io.thalesdigital.schema.Flag
import io.thalesdigital.schema.FlagType
import io.thalesdigital.schema.Schema
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class SchemaTest {

    @Test
    fun should_reject_unknown_parameter() {
        val commandLineArguments = CommandLine().withOption("foo")
        val schema = Schema().with("p")
        Assertions.assertTrue(schema.consume(commandLineArguments).isError())
    }

    @Test
    fun should_accept_flag() {
        val commandLineArguments = CommandLine().withOption("p")
        val schema = Schema().with("p").with("a")
        Assertions.assertFalse(schema.consume(commandLineArguments).isError())
    }

    private fun CommandLine.withOption(s: String): CommandLine {
        return this.with(Argument.option(s))
    }

    private fun Schema.with(s: String): Schema {
        return this.with(Flag(s, FlagType.Boolean))
    }



}
