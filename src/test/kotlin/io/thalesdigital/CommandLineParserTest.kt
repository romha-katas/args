package io.thalesdigital;

import io.thalesdigital.parser.ParsingError
import io.thalesdigital.parser.parseCommandLine
import io.thalesdigital.resolver.Argument
import io.thalesdigital.resolver.CommandLine
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class CommandLineParserTest {

    @Test
    fun `should build an empty command line if no parameter`() {
      "".should_produce(CommandLine())
    }

    @Test
    fun `should build a command line with single flag`() {
        "-p".should_produce(CommandLine().with(Argument("p", true)))
    }

    @Test
    fun `should build a command line with single flag and value`() {
        "-p 12".should_produce(CommandLine().with(Argument("p", true)).with(Argument("12", false)))
    }

    @Test
    fun `should build a command line with multiple flags and values`() {
        "-p 12 -b -l abc".should_produce(CommandLine()
            .with(Argument("p", true))
            .with(Argument("12", false))
            .with(Argument("b", true))
            .with(Argument("l", true))
            .with(Argument("abc", false))
        )
    }

    @Test
    fun `should build a command line with negative number`() {
        "-p -12".should_produce(CommandLine().with(Argument("p", true)).with(Argument("-12", false)))
    }

    @Test
    fun `should reject a value with no flag`() {
        "p".should_raise_an_error()
    }

    @Test
    fun `should reject a missing flag name`() {
        "-p -".should_raise_an_error()
    }

    @Test
    fun `should build a flag with long name`() {
        "-foo -12".should_produce(CommandLine().with(Argument("foo", true)).with(Argument("-12", false)))
    }

    @Test
    fun `should build a flag with a list`() {
        "-f 1,2,3".should_produce(CommandLine().with(Argument("f", true)).with(Argument("1,2,3", false)))
    }
}

private fun String.should_produce(expected: CommandLine) {
    val produced = parseCommandLine(this)

    Assertions.assertEquals(expected.empty(), produced.empty())
    while (!expected.empty()) {
        Assertions.assertFalse(produced.empty())
        val expectedArgument = expected.next()
        val resultArgument = produced.next()

        Assertions.assertEquals(expectedArgument, resultArgument)
    }
}

private fun String.should_raise_an_error() {
    Assertions.assertThrows(ParsingError::class.java) {parseCommandLine(this)}
}
