package io.thalesdigital

class ArgumentResolverException(message: String): RuntimeException(message) {
}
