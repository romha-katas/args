package io.thalesdigital.arguments

import io.thalesdigital.schema.Flag

data class ProgramArgument(val flag: Flag, val value: () -> Any)
