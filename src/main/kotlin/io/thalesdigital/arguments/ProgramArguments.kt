package io.thalesdigital.arguments

import io.thalesdigital.schema.Flag

class ProgramArguments {
    private val mapping = mutableMapOf<Flag, () -> Any>()

    fun add(programArgument: ProgramArgument) {
        mapping[programArgument.flag] = programArgument.value
    }

    fun asBoolean(flag: Flag): Boolean {
        return mapping.contains(flag)
    }

    fun asInteger(flag: Flag): Int {
        return (mapping.getOrDefault(flag) { 0 })() as Int
    }

    fun asIntegerList(flag: Flag): List<Int> {
        @Suppress("UNCHECKED_CAST")
        return ((mapping.getOrDefault(flag) { listOf<Int>() })() as List<Int>)
    }
}
