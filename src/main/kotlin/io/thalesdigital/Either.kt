package io.thalesdigital

interface Either<E, V> {
    fun isError(): Boolean

    fun <U> fold(onError: (error: E) -> U, onValue: (value: V) -> U): U

    companion object {
    fun <E, V> error(value: E): Error<E, V> {
        return Error(value)
    }

    fun <E, V> value(value: V): Value<E, V> {
        return Value(value)
    }
}



class Error<E, V>(val value: E): Either<E, V> {
    override fun isError(): Boolean {
        return true
    }

    override fun <U> fold(onError: (error: E) -> U, onValue: (value: V) -> U): U {
        return onError(value)
    }
}

class Value<E, V>(val value: V): Either<E, V> {
    override fun isError(): Boolean {
        return false
    }

    override fun <U> fold(onError: (error: E) -> U, onValue: (value: V) -> U): U {
        return onValue(value)
    }
}

}
