package io.thalesdigital.parser

import io.thalesdigital.resolver.Argument
import io.thalesdigital.resolver.CommandLine

fun parseCommandLine(s: String): CommandLine {
        var commandLine = CommandLine()

        val characters = s.toCharacters()
        while (characters.isNotEmpty()) {
            characters.readDelimiter()
            val character = characters.next()
            if (character != "-") {
                throw ParsingError("Expecting a flag.")
            }
            commandLine = commandLine.with(Argument(characters.readFlagName(), true))

            characters.readDelimiter()

            val value = characters.readValue()
            if (value.isNotBlank()) {
                commandLine = commandLine.with(Argument(value, false))
            }
        }

        return commandLine
    }


private fun MutableList<String>.seeingNegativeNumber(): Boolean {
    return this.size >= 2 && this.subList(0, 2).joinToString("") { it }.toIntOrNull() != null

}


private fun MutableList<String>.readValue(): String {
    if (isEmpty()) {
        return ""
    }
    var name = ""
    while (this.isNotEmpty() && !this.first().isDelimiter()) {
        name += if (seeing("-")) {
            if (!seeingNegativeNumber()) {
                return ""
            }
            this.next() + readNumber()
        } else {
            this.next()
        }
    }
    return name
}

private fun MutableList<String>.readNumber(): String {
    var number = this.next()
    while (this.isNotEmpty() && this.first().toIntOrNull() != null) {
        val c = this.next()
        number += c
    }
    return number
}

private fun MutableList<String>.readFlagName(): String {
    if (isEmpty()) {
        throw ParsingError("Expecting a flag name. But no character")
    }
    var name = this.next()
    while (this.isNotEmpty() && !this.first().isDelimiter()) {
        name += this.next()
    }
    return name
}

private fun MutableList<String>.readDelimiter() {
    if (isEmpty()) {
        return
    }

    while (this.seeing(" ")) {
        next()
    }
}

private fun MutableList<String>.next() = this.removeFirst()

private fun MutableList<String>.seeing(s: String) = this.first() == s

private fun String.toCharacters(): MutableList<String> {
    return this.split("").filter { it != "" }.toMutableList()
}

private fun String.isDelimiter(): Boolean {
    return this == " "
}
