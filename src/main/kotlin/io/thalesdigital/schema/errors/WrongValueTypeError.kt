package io.thalesdigital.schema.errors

class WrongValueTypeError(msg: String): SchemaError(msg) {
}
