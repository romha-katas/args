package io.thalesdigital.schema.errors

import io.thalesdigital.resolver.Argument

class UnexpectedValueError(argument: Argument) : SchemaError("Expected an flag but got the value '${argument.value}'") {

}
