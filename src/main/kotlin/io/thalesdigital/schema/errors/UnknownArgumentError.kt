package io.thalesdigital.schema.errors

import io.thalesdigital.resolver.Argument

class UnknownArgumentError(argument: Argument): SchemaError("The argument ${argument.value} is unknown")
