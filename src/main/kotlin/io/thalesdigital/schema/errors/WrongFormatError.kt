package io.thalesdigital.schema.errors

import io.thalesdigital.schema.Flag

class WrongFormatError(flag: Flag, msg: String): SchemaError("Cannot retrieve value for ${flag.name}: $msg") {
}
