package io.thalesdigital.schema

import io.thalesdigital.*
import io.thalesdigital.resolver.CommandLine
import io.thalesdigital.schema.errors.SchemaError
import io.thalesdigital.schema.errors.WrongValueTypeError

private fun CommandLine.nextInt(): Either<SchemaError, () -> Any> {
    return try {
        val value = next().value
        val parsed = value.toInt()
        Either.value { parsed }
    } catch(e: NumberFormatException) {
        Either.error(WrongValueTypeError("The value '$this' is not an integer"))
    }
}

private fun CommandLine.nextIntList(): Either<SchemaError, () -> Any> {
    return try {
        val values = next().asList().map { it.toInt() }
        Either.value { values }
    } catch(e: NumberFormatException) {
        Either.error(WrongValueTypeError("The value '$this' is not an list of integers"))
    }
}


enum class FlagType(val marshal: (c: CommandLine) -> Either<SchemaError, () -> Any>) {
    Boolean({ _ -> Either.value { true } }),
    Integer({ c: CommandLine -> c.nextInt() }),
    Integers({ c: CommandLine -> c.nextIntList() })
}
