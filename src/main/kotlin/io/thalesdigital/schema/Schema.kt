package io.thalesdigital.schema

import io.thalesdigital.*
import io.thalesdigital.arguments.ProgramArgument
import io.thalesdigital.resolver.CommandLine
import io.thalesdigital.schema.errors.SchemaError
import io.thalesdigital.schema.errors.UnexpectedValueError
import io.thalesdigital.schema.errors.UnknownArgumentError
import io.thalesdigital.schema.errors.WrongFormatError

class Schema private constructor(private val flags: List<Flag>) {

    constructor(): this(emptyList<Flag>())

    fun with(s: Flag): Schema {
        if (flags.find { it.name == s.name } != null) {
            throw NotUniqueFlagException(s.name)
        }
        return Schema(flags + listOf(s))
    }

    fun consume(arguments: CommandLine): Either<SchemaError, ProgramArgument> {
        val argument = arguments.next()
        if (!argument.isFlagName) {
            return Either.error(UnexpectedValueError(argument))
        }
        val flag = flags.find { it.name == argument.value } ?: return Either.error(UnknownArgumentError(argument))
        return flag.type.marshal(arguments).fold(
            { error -> Either.error(WrongFormatError(flag, error.message)) },
            { value -> Either.value(ProgramArgument(flag, value)) })
    }
}



