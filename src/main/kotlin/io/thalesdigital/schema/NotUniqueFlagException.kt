package io.thalesdigital.schema

class NotUniqueFlagException(p: String): RuntimeException("The parameter '$p' already exists") {
}
