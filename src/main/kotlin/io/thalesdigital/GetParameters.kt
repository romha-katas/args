package io.thalesdigital

import io.thalesdigital.arguments.ProgramArguments
import io.thalesdigital.parser.parseCommandLine
import io.thalesdigital.resolver.ArgumentResolver
import io.thalesdigital.schema.Schema

class GetParameters(val schema: Schema) {
    operator fun invoke(commandLineString: String): ProgramArguments {
        val commandLine = parseCommandLine(commandLineString)
        val resolver = ArgumentResolver(schema)

        return resolver(commandLine).fold(
            {throw ArgumentResolverException(it.message)},
            {it}
        )
    }
}
