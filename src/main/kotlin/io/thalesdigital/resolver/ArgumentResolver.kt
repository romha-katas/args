package io.thalesdigital.resolver

import io.thalesdigital.Either
import io.thalesdigital.arguments.ProgramArguments
import io.thalesdigital.schema.Schema
import io.thalesdigital.schema.errors.SchemaError

class ArgumentResolver(val schema: Schema) {

    operator fun invoke(commandLine: CommandLine): Either<SchemaError, ProgramArguments> {
        return resolve(commandLine, ProgramArguments())
    }

    private fun resolve(commandLineCommandLine: CommandLine, flags: ProgramArguments): Either<SchemaError, ProgramArguments> {
        if (commandLineCommandLine.empty()) {
            return Either.value(flags)
        }
        return accept(commandLineCommandLine, flags)
    }

    private fun accept(commandLine: CommandLine, flags: ProgramArguments): Either<SchemaError, ProgramArguments> {
        val flagValue = schema.consume(commandLine)
        return flagValue.fold( {
            error -> Either.error(error)
        }, { value ->
            flags.add(value)
            resolve(commandLine, flags)
        })
    }
}
