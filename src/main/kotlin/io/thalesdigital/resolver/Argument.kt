package io.thalesdigital.resolver


data class Argument(val value: String, val isFlagName: Boolean) {
    companion object {
        fun option(value: String): Argument {
            return Argument(value, true)
        }
        fun value(value: String): Argument {
            return Argument(value, false)
        }
    }

    fun asList(): List<String> {
        return value.split(",").map { it }
    }

}
