package io.thalesdigital.resolver

class CommandLine private constructor(private val arguments: List<Argument>) {
    private var index = arguments.iterator()

    constructor(): this(emptyList())

    fun empty(): Boolean {
        return !index.hasNext()
    }

    fun next(): Argument {
        return index.next()
    }

    fun with(argument: Argument): CommandLine {
        return CommandLine(arguments + listOf(argument))
    }
}
